package main

import (
	"encoding/json"
	"fmt"
	"testing"
	"github.com/google/uuid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
    "github.com/stretchr/testify/assert"
)	



func TestInvokeFunction(t *testing.T) {
	fmt.Println("Executing ijjnvoke functions")
	

	// testdata  := []asset{}
	assert := assert.New(t)

	// Instantiate mockStub using AidChaincode as the target chaincode to unit test
	stub := shim.NewMockStub("TestStub", new(StockChaincode))
	assert.NotNil(stub, "Stub is nil, Test stub creation failed")
// Refresh Available funds under Project
	// result1 := stub.MockInvoke(uid,
	// 	[][]byte{[]byte("asset-read")})

	// fmt.Println(string(result1.GetPayload()))

	// assert.EqualValues(shim.OK, result1.GetStatus(), " failed to read the project data")
	uid:=uuid.New().String()

	typeOf := LC{"Type","kailash","ajaya","kailashBank","ajayaBank","hhhh","ABC",10000,"WAITING"}
	// ast1 := asset{"Asset","Lptop", "This is a Leno Laptop", "n-perishable", 5, 90000, 11111,111111, "ajaya"}

	typeAsBytes, _ := json.Marshal(typeOf)
	
	result := stub.MockInvoke(uid,[][]byte{[]byte("create-LC"), typeAsBytes})
	
	fmt.Println(result)
	assert.EqualValues(shim.OK, result.GetStatus(), " failed to read ocreject data")
	partial:=struct{
		ProductID  string `json:"productID"`
		EntityName string `json:"entityName"`
		State      string `json:"state"`
	}{"hhhh","kailashBank","APPROVED"}
	partialAsBytes,_:=json.Marshal(partial)
	result3 := stub.MockInvoke(uid,
		[][]byte{[]byte("update-LC-Status"),partialAsBytes})

	fmt.Println(string(result3.GetPayload()))

	assert.EqualValues(shim.OK, result3.GetStatus(), " failed to read oject data")
	



	partial1:=struct{
		EntityName string `json:"entityName"`
	}{"ajayaBank"}

	partialAsBytes,_=json.Marshal(partial1)
	result3 = stub.MockInvoke(uid,
		[][]byte{[]byte("read-LC"),partialAsBytes})

	fmt.Println(string(result3.GetPayload()))

	assert.EqualValues(shim.OK, result3.GetStatus(), " failed to read the project data")

	

}