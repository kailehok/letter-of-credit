package main

import (
	"encoding/json"
	"fmt"

	

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//CreateLC  creates type of the variable
func CreateLC(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	lc := LC{}

	err := json.Unmarshal([]byte(args[0]), &lc)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Create asset key
	LcKey, err := stub.CreateCompositeKey("LC", []string{lc.ProductID})
	if err != nil {
		return shim.Error(err.Error())
	}

	//Check if data exists for this key
	val, err := stub.GetState(LcKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val != nil {
		return shim.Error("LC already exists")
	}

	//value as bytes to put in the ledger
	lcAsBytes, err := json.Marshal(lc)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(LcKey, lcAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println(lc)

	return shim.Success(lcAsBytes)

}

//ReadLC structure
func ReadLC(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 {
		return shim.Error("Expected no arguments")

	}

	partial := struct {
		EntityName string `json:"entityName"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("LC", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		//Construct response struct
		result := struct {
			LcKey               string `json:"lcKey"`
			Date                string `json:"date"`
			ImporterName        string `json:"importerName"`
			ExporterName        string `json:"exporterName"`
			ImporterBankName    string `json:"importerBankName"`
			ExporterBankName    string `json:"exporterBankName"`
			ProductID           string `json:"productID"`
			ProductOrderDetails string `json:"productOrderDetails"`
			ProductAmt          uint64 `json:"productAmt"`
			State               string `json:"state"`
		}{}

		
	
		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}
		if  !(partial.EntityName == result.ExporterBankName || partial.EntityName == result.ImporterBankName){
			continue
		}

		if (partial.EntityName == result.ExporterBankName){
			if (result.State !="APPROVED"){
				continue
			}
		}

		

		//Fetch Key
		result.LcKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println(results)
	return shim.Success(resultsAsBytes)
}

//UpdateLCStatus  creates type of the variable
func UpdateLCStatus(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		ProductID  string `json:"productID"`
		EntityName string `json:"entityName"`
		State      string `json:"state"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Create asset key
	LcKey, err := stub.CreateCompositeKey("LC", []string{partial.ProductID})
	//Check if data exists for this key
	val, err := stub.GetState(LcKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		fmt.Println("LC doesnt exists")
		return shim.Error("LC doesnt exists")
	}

	lc := LC{}
	err = json.Unmarshal(val, &lc)
	if err != nil {
		return shim.Error(err.Error())

	}

	if lc.ImporterBankName == partial.EntityName || lc.ExporterBankName == partial.EntityName {
		//Create asset key
		LcKey, err := stub.CreateCompositeKey("LC", []string{partial.EntityName, partial.ProductID})
		lc.State = partial.State
		//value as bytes to put in the ledger
		lcAsBytes, err := json.Marshal(lc)
		//Put the key value in ledger
		err = stub.PutState(LcKey, lcAsBytes)
		if err != nil {
			return shim.Error("Error marshalling the data")
		}
	} else {
		fmt.Println("Incorrect entity name")

		return shim.Error("Incorrect entity name")

	}

	lc.State = partial.State
	//value as bytes to put in the ledger
	lcAsBytes, err := json.Marshal(lc)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(LcKey, lcAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println(lc)


	return shim.Success(lcAsBytes)

}
