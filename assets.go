package main

//LC structure
type LC struct{
	Date string `json:"date"`
	ImporterName string `json:"importerName"`
	ExporterName string `json:"exporterName"`
	ImporterBankName string `json:"importerBankName"`
	ExporterBankName string `json:"exporterBankName"`
	ProductID string `json:"productID"`
	ProductOrderDetails string `json:"productOrderDetails"`
	ProductAmt uint64 `json:"productAmt"`
	State string `json:"state"`
}
